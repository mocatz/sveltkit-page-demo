# create-svelte

Everything you need to build a Svelte project, powered by [`create-svelte`](https://github.com/sveltejs/kit/tree/master/packages/create-svelte);

## Creating a project

If you're seeing this, you've probably already done this step. Congrats!

```bash
# create a new project in the current directory
npm init svelte@next

# create a new project in my-app
npm init svelte@next my-app
```

> Note: the `@next` is temporary

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

Before creating a production version of your app, install an [adapter](https://kit.svelte.dev/docs#adapters) for your target environment. Then:

```bash
npm run build
```

> You can preview the built app with `npm run preview`, regardless of whether you installed an adapter. This should _not_ be used to serve your app in production.


## Setup for gitlab pages

⚠️ the page with the todo list won't work (cors issue or something i don't understand)

### 1 Add gitlab CI

+ Add `.gitlab-ci.yml` file in root project
+ Set content to:

````yaml
image: node:14

pages:
  stage: deploy
  cache:
    key:
      files:
        - package-lock.json
      prefix: npm
    paths:
      - node_modules/
  script:
    - npm install
    - npm run build
    - cp -a build/. public/
  artifacts:
    paths:
      - public
  only:
    - master

````

### 2 Add adapter-static && update svelte config

+ Install adapter-static: `npm i -D @sveltejs/adapter-static@next`
+ Update svelte.config.js
````js
import adapter from '@sveltejs/adapter-static';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	kit: {
		adapter: adapter(),
		paths: {
			base: process.env.NODE_ENV !== 'production'? '' : '/<your repo name>',
		},
		// hydrate the <div id="svelte"> element in src/app.html
		target: '#svelte'
	}
};

export default config;
````
### 3 Update all links in <a>

for example in the original header component:
`<li class:active={$page.path === '/'}><a sveltekit:prefetch href="/">Home</a></li>`
Add a `.` behind the path in href
`<li class:active={$page.path === '/'}><a sveltekit:prefetch href="./">Home</a></li>`

### 4 Run the build locally

Check if the build is ok before (Just in case)
`npm run build`

### 5 Push and launch the CI

+ `git push`
+ Then go to your gitlab page `https://<your account name>.gitlab.io/<your repo name>/`



**If you get the error** sh: svelte-kit: command not found
+ delete node_modules 
+ And reinstall the packages `npm install`

